# Teste - FH

## 1) Iniciando o ambiente

### 1.1 Desenvolvimento:
1. Abra o root do projeto
2. Instale as dependências com `npm install`
3. Instale o Gulp global com `npm install gulp -g`
4. Inicie o ambiente de desenvolvimento com `gulp watch`

### 1.2 Produção (local):
1. Abra o root do projeto
2. Instale as dependências com `npm install`
3. Instale o Gulp global com `npm install gulp -g`
4. Inicie o ambiente de desenvolvimento com `gulp build:local`

### 1.3 Produção (host):
1. Abra o root do projeto
2. Instale as dependências com `npm install`
3. Instale o Gulp global com `npm install gulp -g`
4. Inicie o ambiente de desenvolvimento com `gulp build:production`


## 2) Diário de dev

Estou escrevendo enquanto desenvolvo, como um diário de desenvolvimento.

Primeiramente eu abri o ambiente de desenvolvimento dado pela empresa, achei um pouco bagunçado, tive que abrir 3 terminais lado a lado para conseguir executar os cmds necessários. 

A primeira coisa que fiz foi gerar o build local, depois executar o watch do css/js e posteriormente o liveReload.

Seria muito mais fácil se tivesse uma task só, com um browserSync stream, junto com gulp-changed. Ao detectar uma mudança, ele rebuilda os arquivos e da o reload. É muito mais rápido do que gulp watch.

A segunda coisa que pensei, foi em refazer o ambiente desenvolvimento para melhorar o flow. Pensei em utilizar um boilerplate que eu mesmo fiz utilizando gulp + webpack + babel + es6, porém, não tenho tanto tempo (posso disponibiliza-lo posteriormente em um repositório publico).

Quando consegui me acostumar ao ambiente de desenvolvimento, eu percebi que os arquivos estavam sem extensão, só consegui abrir a imagem da planilha e tive que imaginar como seria o layout da página.

Solicitei o arquivo que não estava conseguindo abrir.

Trabalhei na modelagem de dados do JSON, essa parte é tranquila, é só imaginar como seria preenchido os dados na tela e posteriormente, como popular. Então, você tem um modelo de dados bem implementado. 

Utilizei o número do endereço da residência como string, pois o mesmo pode conter alguma letra, exemplo: "12A"

Não temos o valor total do pedido no JSON, pois ele deve ser calculado pelo client-side.

Acabado o JSON, preciso consumi-lo através do JS.

Eu sou contra a utilização de jQuery, apesar de não ser um problema para mim. Porém, prefiro utilizar VanillaJS.

Antes de iniciar eu preciso criar a estrutura que vou dar append.

Como um dos arquivos não abriu, eu não sei exatamente se existe um layout a ser seguido, porém, iniciei a estrutura padrão do SASS (design pattern).

Utilizarei como arquitetura de CSS, BEM CSS. É o meu pattern favorito.

Acabei mexendo bastante no SASS agora, importei a fonte e joguei tudo em um repositório público.

Pensando em como fazer o layout, acabei optando por fazer 2 colunas centralizadas. Uma com os pedidos e outra com o detalhes do pedido selecionado.

Consumido o json com XMLHttpRequest no JS, hora de popular os dados.

Botões estilizados e bindados com as funções de cada um, hora de preencher o pedido conforme selecionado na lista.

Na hora de criar o card que vai holdar as informações, eu notei que o bootstrap não contém alguns componentes que o projeto original possui. Pode ser a versão, pois o mesmo utiliza a versão 3.3.7, de qualquer forma, eu acabei refazendo o component

Feito o preenchimento da tabela e a criação do card, entregue.